classdef DobotSimulation < handle
    % Latest Revision - 21 May 2020
    % Student Number: 11686122
    
    properties
        % Dimensions of the workspace in regard to the entire simulation
        % space encompassing table, robots, guards, etc.
        workspace = [2 -2 2 -2 2 -0.5];
        
        % Transform for centre of enviroment
        centreTr = eye(4);
        
        % Determine a scale for skeleton frame sizing of joints in
        % robot.plot
        scale = 0.1;
        
        % Neutral joint state for dobot arm
        q = zeros(1,7);
        
        % Neutral joint state for dobot arm
        qRest = [deg2rad(0) deg2rad(80) deg2rad(-85) deg2rad(5) -0.052 0 -0.06];
        
        % Neutral joint state for dobot arm
        qRailRest = [0 deg2rad(0) deg2rad(80) deg2rad(-85) deg2rad(5) -0.052 0.00 -0.06];       
    end
    
    methods(Static)
        
        %% ClearEnviroment
        % Clear all previous figures, set axis, activate or deactivate
        % surroundings import all necessary singular objects/ply files to build
        % the environment
        function ClearEnvironment()
            
            % clear all windows and dock figure
            clf;
            clc;
            set(0,'DefaultFigureWindowStyle', 'docked')
            
            % show axis labels, square figure and hold for future plots
            xlabel('X');
            ylabel('Y');
            zlabel('Z');
            axis equal;
            hold on;
            
        end
    end
    
    methods %% ... Structors for robot simulation
        
        % Main function that operates the entire simulation
        function self = DobotSimulation()
            
            % Create the surrounding environment workspace, including clearing the
            % figures, command window and workspace variables
            self.ClearEnvironment();
            
            %Import all of the required objects and ply files for simulation
            env = self.GetEnvironment(8,10); 
            
            % Create a Dobot Model
            robot1 = self.GetRobot('Dobot 1','DOBOT LINEAR RAIL', ...
                env.dobotBench.Tr, transl([0.56,0.1045,0.051]), eye(4));
            
            % Plot the Dobot Arm
            self.PlotAndColourRobot(robot1,self.qRailRest);
 
            % Create a Dobot Model
%             robot2 = self.GetRobot('Dobot 2','DOBOT LINEAR RAIL', ...
%                 env.dobotBench.Tr, transl([-0.56,-0.1045,0.051]), trotx(180 * pi/180));
%             
%             % Plot the Dobot Arm
%             self.PlotAndColourRobot(robot2,self.qRailRest);
            
            % Enable adjustment of current model using joint state inputs
            robot1.teach();
            
            save('dobotEnvironment');
            
            %% 
            load('dobotEnvironment');
            PickPlace(self,robot1,env.feta,env.pizza);
            
        end
        
        %% GetEnvironment
        % Import all necessary singular objects/ply files to build
        % the environment
        function env = GetEnvironment(self,tubCount, ingredientCount)
            
            % Import Conveyor Belt
            env.conveyorBelt = GetObject(self,'Conveyor Belt',self.centreTr, ...
                transl([0,0,0.795]),trotz(180 * pi/180));
            
            % Import the bench that the Dobots mount to
            env.dobotBench = GetObject(self,'Dobot Bench',env.conveyorBelt.Tr, ...
                transl([0,0,0.1383]),trotz(90 * pi/180));
            
            % Import the benches that the ingredients are situated on
            env.foodBenchL = GetObject(self,'Secondary Bench',env.dobotBench.Tr, ...
                transl([-0.435,0,-0.125]),trotz(90 * pi/180));
            
            env.foodBenchR = GetObject(self,'Secondary Bench',env.dobotBench.Tr, ...
                transl([0.435,0,-0.125]),trotz(90 * pi/180));
            
            %Create a list of x.y.z coodinates for 8 ingredent tubs to be
            %situated in reference to their parent coordinate/frame of reference
            tubGrid = [0.26717,0.096,0;0.26717,-0.096,0;-0.26717,0.096,0;-0.26717,-0.096,0; ...
                0.41917,0.096,0;0.41917,-0.096,0;-0.41917,0.096,0;-0.41917,-0.096,0];
            
            % Import the ingredient tubs as per the tubCount variable for the Left Bench.
            % Attempt to split the tubCount over left and right benches
            % equally.
            for i = 1:ceil(tubCount/2)
                % Import the ingredient tubs that the ingredients are placed within
                env.foodTub(i) = GetObject(self,'Ingredient Tub',env.foodBenchL.Tr, ...
                    transl(tubGrid(i,:)),trotz(90 * pi/180));
                
                % Import the ingredients required in the left hand
                % foodbench and place within a specified
                % tub in a randomised pose. A total amount of the
                % ingredient is ingredientCount

                % Import Feta
                if i == 1
                    for j = 1:ingredientCount
                        
                        % Create random number distribution for ingredient
                        % pose within tub limits. Limits are specified here in mm and degrees. 
                        [T, R] = Random(self, -40,40,-30,30,-25,60,-90,90);
                        
                        % Import ingredients with the randomised values
                        env.feta(j) = GetObject(self,'Feta',env.foodTub(i).Tr, ...
                            transl([T.px,T.py,T.pz]),R);
                    end
                end
                
                % Import Pepperoni
                if i == 2
                    for j = 1:ingredientCount
                        
                        % Create random number distribution for ingredient
                        % pose within tub limits. Limits are specified here in mm and degrees.
                        [T, R] = Random(self, -40,40,-30,30,-25,60,-90,90);
                        
                        % Import ingredients with the randomised values
                        env.feta(j) = GetObject(self,'Pepperoni',env.foodTub(i).Tr, ...
                            transl([T.px,T.py,T.pz]),R);
                    end
                end
                
                % Import Pineapple
                if i == 3
                    for j = 1:ingredientCount
                        
                        % Create random number distribution for ingredient
                        % pose within tub limits. Limits are specified here in mm and degrees. 
                        [T, R] = Random(self, -40,40,-30,30,-25,60,-90,90);
                        
                        % Import ingredients with the randomised values
                        env.feta(j) = GetObject(self,'Pineapple',env.foodTub(i).Tr, ...
                            transl([T.px,T.py,T.pz]),R);
                    end
                end
                
                % Import Salami
                if i == 4
                    for j = 1:ingredientCount
                        
                        % Create random number distribution for ingredient
                        % pose within tub limits. Limits are specified here in mm and degrees. 
                        [T, R] = Random(self, -40,40,-30,30,-25,60,-90,90);
                        
                        % Import ingredients with the randomised values
                        env.feta(j) = GetObject(self,'Salami',env.foodTub(i).Tr, ...
                            transl([T.px,T.py,T.pz]),R);
                    end
                end
            end
            
            % Import the ingredient tubs as per the tubCount variable for the Left Bench.
            % Attempt to split the tubCount over left and right benches
            % equally.
            for i = ceil(tubCount/2)+1:tubCount
                % Import the ingredient tubs that the ingredients are placed within
                env.foodTub(i) = GetObject(self,'Ingredient Tub',env.foodBenchR.Tr, ...
                    transl(tubGrid(i-ceil(tubCount/2),:)),trotz(90 * pi/180));
            end
            

            env.pizza = GetObject(self,'Pizza Base',env.conveyorBelt.Tr, ...
                transl([-0.35,0,0]),trotz(0 * pi/180));     
        end
        
        %% Random
        % Have a random number generator for translation and rotation
        % functions that is used in both randomly importing ingredients
        % into tubs and also for the robots to randomly place onto the
        % pizza.
        function [T, R] = Random(self, xMin,xMax,yMin,yMax,zMin,zMax,rotMin,rotMax)
        
        % Define randon tranlation intergers in metres to assign to object.
        T.px = randi([xMin xMax],1,1)/1000;
        T.py = randi([yMin yMax],1,1)/1000;
        T.pz = randi([zMin zMax],1,1)/1000;
        
        % Define an a,y,z random pose matrix to assign to an object.
        R = trotx(randi([rotMin rotMax],1,1) * pi/180)...
            * trotz(randi([rotMin rotMax],1,1) * pi/180)...
            * troty(randi([rotMin rotMax],1,1)* pi/180);
        end
        
        %% GetObject
        % Import the ply files that were made in blender for the environmental objects.
        % Similiar to the PlotAndColourRobot function, the objects will have associated
        % Tr, colour, vertex, faces and ply data that will be used later for collision avoidance
        function obj = GetObject(~,name, parentTr, moveTr, rotateTr)
            if nargin < 1
                obj.name = ['OBJECT_',datestr(now,'HHMM_SSFFF')];
            end
            obj.name = name;
            [obj.f,obj.v,obj.data] = plyread(['../CAD/Ply Files/',name,'.ply'],'tri');
            
            % Scale the colours to be 0-to-1 (they are originally 0-to-255)
            obj.vertexColours = [obj.data.vertex.red, obj.data.vertex.green, obj.data.vertex.blue] / 255;
            
            % Get vertex count
            obj.vertexCount = size(obj.v,1);
            
            % Create a transform to describe the location (at the origin, since it's centered
            % Move the pose and  rotate if required from the parent pose object
            obj.Tr = parentTr * moveTr * rotateTr;
            
            % Output the updated points for the new position
            obj.updatedPoints = (obj.Tr * [obj.v,ones(obj.vertexCount,1)]')';
            
            % Then plot the trisurf
            obj.mesh_h = trisurf(obj.f,obj.v(:,1),obj.v(:,2),obj.v(:,3) ...
                ,'FaceVertexCData',obj.vertexColours,'EdgeColor','none','EdgeLighting','flat');
            
            % If cam light is off turn on
            if isempty(findobj(get(gca,'Children'),'Type','Light'))
                camlight;
            end
            
            % Now update the vertex points
            obj.mesh_h.Vertices = obj.updatedPoints(:,1:3);
        end
        
        %% GetRobot
        % Given a name (optional), create and return a UR5 robot model
        function robot = GetRobot(~,name,type,parentTr, moveTr, rotateTr)
            if type == "DOBOT ARM"
                
                if nargin < 2
                    % Create a unique name (ms timestamp after 1ms pause)
                    pause(0.000001);
                    name = ['DOBOT_MAGICIAN_',datestr(now,'yyyymmddTHHMMSSFFF')];
                end
                
                % Define the DH parameters to create the kinematic model -
                % DOBOT MAGICIAN WITH A TWO CLAW GRIPPER
                L1 = Link('d',0.139,'a',0,'alpha',pi/2,'offset',0,'qlim',[deg2rad(-180),deg2rad(180)]);
                L2 = Link('d',0,'a',0.135,'alpha',0,'offset',0,'qlim',[deg2rad(-15),deg2rad(110)]);
                L3 = Link('d',0,'a',0.147,'alpha',0,'offset',0,'qlim',[deg2rad(-160),deg2rad(-12)]);
                L4 = Link('d',0,'a',0.0584,'alpha',pi/2,'offset',0,'qlim',[deg2rad(-90),deg2rad(110)]);
                L5 = Link('theta',deg2rad(0),'a',0,'alpha',pi/2,'offset',0.135,'qlim',[-0.052 0]);
                L6 = Link('theta',deg2rad(-90),'a',-0.0305,'alpha',0,'offset',0,'qlim',[-0.03 0]);
                L7 = Link('theta',deg2rad(180),'a',0,'alpha',0,'offset',0.06,'qlim',[-0.06 0]);
                
                % Create the dobot arm model
                robot = SerialLink([L1 L2 L3 L4 L5 L6 L7],'name', name);
                
                % Position the base of the ARM to specified pose
                robot.base = parentTr * moveTr * rotateTr;
                
            elseif type == "DOBOT LINEAR RAIL"
                
                if nargin < 2
                    % Create a unique name (ms timestamp after 1ms pause)
                    pause(0.000001);
                    name = ['DOBOT_LINEAR_RAIL_',datestr(now,'yyyymmddTHHMMSSFFF')];
                end
                
                % Define the DH parameters to create the kinematic model -
                % DOBOT LINEAR RAIL
                R1 = Link('prismatic','theta',deg2rad(90),'a',0,'alpha',pi/2,'offset',1.12,'qlim',[-1.12 0]);
                L1 = Link('d',0.1595,'a',0,'alpha',pi/2,'offset',0,'qlim',[deg2rad(-180),deg2rad(180)]);
                L2 = Link('d',0,'a',0.135,'alpha',0,'offset',0,'qlim',[deg2rad(-15),deg2rad(110)]);
                L3 = Link('d',0,'a',0.147,'alpha',0,'offset',0,'qlim',[deg2rad(-160),deg2rad(-12)]);
                L4 = Link('d',0,'a',0.0584,'alpha',pi/2,'offset',0,'qlim',[deg2rad(-90),deg2rad(110)]);
                L5 = Link('prismatic','theta',deg2rad(0),'a',0,'alpha',pi/2,'offset',0.135,'qlim',[-0.052 0]);
                L6 = Link('prismatic','theta',deg2rad(-90),'a',-0.0305,'alpha',0,'offset',0,'qlim',[-0.03 0]);
                L7 = Link('prismatic','theta',deg2rad(180),'a',0,'alpha',0,'offset',0.06,'qlim',[-0.06 0]);
                
                % Create the linear rail model
                robot = SerialLink([R1 L1 L2 L3 L4 L5 L6 L7],'name', name);
                
                % Position the base of the LINEAR RAIL to specified pose;
                % An initial offset rotation is required to get the robot
                % sitting upright first.
                robot.base = parentTr * moveTr * troty(-90 * pi/180) * rotateTr;
            end
        end
        
        %% PlotAndColourRobot
        % Given a robot index, add the glyphs (vertices and faces) and
        % colour them in if data is available
        function PlotAndColourRobot(self,robot,q)%robot,workspace)
            
            % Update the joint states of gripper tool
            % to be parallel to base and symettric in movement of both claws
             q = DobotCorrection(self,robot,q,"CLOSED");
            
            % If the robot has less than 8 links it is without the dobot
            % rail and must use a slightly different set of ply files
            if robot.n < 6
                % read in robot data from ply file
                for linkIndex = 0:robot.n
                    [faceData, vertexData, plyData{linkIndex+1}] = ...
                        plyread(['../CAD/Ply Files/Dobot Magician L',num2str(linkIndex),'.ply'],'tri'); %#ok<AGROW>
                    
                    % extract face and vertex data
                    robot.faces{linkIndex+1} = faceData;
                    robot.points{linkIndex+1} = vertexData;
                end
                
            else
                % read in robot data from ply file
                for linkIndex = 0:robot.n
                    [faceData, vertexData, plyData{linkIndex+1}] = ...
                        plyread(['../CAD/Ply Files/Dobot Link ',num2str(linkIndex),'.ply'],'tri'); %#ok<AGROW>
                    % extract face and vertex data
                    robot.faces{linkIndex+1} = faceData;
                    robot.points{linkIndex+1} = vertexData;
                end
            end
            
            % plot the robot with the workspace dimensions
            robot.plot3d(q,'workspace',self.workspace);
            
            % if camera light is off turn on
            if isempty(findobj(get(gca,'Children'),'Type','Light'))
                camlight;
            end
            robot.delay = 0.000000001;
            
            % Try to correctly colour the arm (if colours are in ply file data)
            for linkIndex = 0:robot.n
                handles = findobj('Tag', robot.name);
                h = get(handles,'UserData');
                try
                    h.link(linkIndex+1).Children.FaceVertexCData = [plyData{linkIndex+1}.vertex.red ...
                        , plyData{linkIndex+1}.vertex.green ...
                        , plyData{linkIndex+1}.vertex.blue]/255;
                    h.link(linkIndex+1).Children.FaceColor = 'interp';
                catch ME_1
                    disp(ME_1);
                    continue;
                end
            end
        end
        
        %% DobotCorrection
        % The Dobot arm design ensure the end effector always remains
        % parallel with L1, therefore Dobot Joint ensures this is so. The
        % RH claw also needs to mimic the q joint state of the LH claw but
        % in the opposite direction.
        function q = DobotCorrection(~,robot,q,gripper)
            % Account for whether the robot is a single dobot arm or a
            % dobot arm mounted to the rail, the dobot arm only has 7 seven
            % joints. When the rail is included it becomes 8
            if size(q,2) == 7
                q(4) = 0-q(2)-q(3);
                if gripper == "CLOSED"
                    q(6) = 0;
                    q(7) = -robot.offset(7);
                elseif gripper == "OPEN"
                    q(7) = -robot.offset(7)-(2*q(6));
                end
  
            else
                q(5) = 0-q(3)-q(4);
                if gripper == "CLOSED"
                    q(7) = 0;
                    q(8) = -robot.offset(8);
                elseif gripper == "OPEN"
                    q(8) = -robot.offset(8)-(2*q(7));
                end
            end
        end
  
        %% PickPlace
        % Given a starting point and a target point for an object, find a
        % suitable smooth trajectory that is collision free to pick and
        % place the object using the dobot
        function PickPlace(self,robot,object,destination)
            
            % Set dropHeight in m.
            pickupHeight = 0.060;
            
            dropHeight = 0.010;
                                 
            MoveRobot(self,robot,object.Tr,"CLOSED");
                       
            MoveRobot(self,robot,destination.Tr*transl([0,0,pickupHeight]),"CLOSED");
            
            MoveRobot(self,robot,destination.Tr*transl([0,0,dropHeight]),"CLOSED");
            
            MoveRobot(self,robot,destination.Tr*transl([0,0,dropHeight]),"OPEN");
            
            MoveRobot(self,robot,destination.Tr*transl([0,0,pickupHeight]),"OPEN");

        end     
             
        %% MoveRobot
        % Move the robot, from one pose to another keeping within
        % joint limits and following a collision free path
        function MoveRobot(self,robot,nextTr,gripper)

            % Get the goal joint state
            goalJointState = robot.getpos();
            actualTr = robot.fkine(goalJointState);
            
            minDistError = sqrt(sum(sqrt(sum((actualTr - nextTr).^2)).^2));
            bestJointState = goalJointState;
            maxAllowableDistError = 0.0001;
            iKineAlpha = 0.9;
            
            % Perform inverse kine
            while maxAllowableDistError < minDistError && 0 < iKineAlpha
                candidateJointState = robot.ikine(nextTr,bestJointState,[1,1,1,0,0,0],'alpha',iKineAlpha,'ilimit',5);
                           
%                 % Fix joint limits 
%                 if ~all(robot.qlim(:,1) < candidateJointState'  &  candidateJointState' < robot.qlim(:,2))
%                     lessThanMinIndex = candidateJointState' < robot.qlim(:,1);
%                     greaterThanMaxIndex = robot.qlim(:,2) < candidateJointState';
%                     candidateJointState(lessThanMinIndex) = robot.qlim(lessThanMinIndex,1) + 0.1*pi/180;
%                     candidateJointState(greaterThanMaxIndex) = robot.qlim(greaterThanMaxIndex,2) - 0.1*pi/180;
                    
%                     disp('Not in joint limits, so fixing');
%                 end
                
                actualTr = robot.fkine(candidateJointState);
                iKineAlpha = iKineAlpha - 0.0005;
                currentDistError = sqrt(sum(sqrt(sum((actualTr - nextTr).^2)).^2));
                
                % If less than the best so far and within joint limits
                if  currentDistError < minDistError
                    minDistError = currentDistError;
                    bestJointState = candidateJointState;
                end
                bestJointState = DobotCorrection(self,robot,bestJointState,gripper);
            end
            
            goalJointState = bestJointState;
            
            % Fix goal pose back to a small step away from the min/max joint limits
            fixIndexMin = goalJointState' < robot.qlim(:,1);
            goalJointState(fixIndexMin) = robot.qlim(fixIndexMin,1) + 0.01*pi/180;
            fixIndexMax = robot.qlim(:,2) < goalJointState';
            goalJointState(fixIndexMax) = robot.qlim(fixIndexMax,2) - 0.01*pi/180;
            
            jSteps = 20;
            q1 = robot.getpos();
            q2 = goalJointState;
            
            %Determine the number of jSteps required through a qunitic
            %polynomial profile to acheive <1 degree movements.
            while ~isempty(find(1 < abs(diff(rad2deg(jtraj(q1,q2,jSteps)))),1))
                jSteps = jSteps + 1;
            end
                
            % Get a trajectory by Quintic Polynomial Profile
            % create trajectory of goal steps to aheive the moment
            %jointTrajectory = jtraj(q1,q2,jSteps);
            
            % OR the better way...  Get a trajectory by Trapezoidal Profile
            % First, create the scalar function that varies smoothly from
            % state 1 to 2
            trap_s = lspb(0,1,jSteps);
            
            % Create memory allocation for variables
            qMatrix = nan(jSteps,8);
            
            % Generate interpolated joint angles
            for i = 1:jSteps
                qMatrix(i,:) = (1-trap_s(i))*q1 + trap_s(i)*q2;
            end
            for i = 1:jSteps
                
                %Animate the robot arm
                robot.animate(qMatrix(i,:));
            end     
        end
        
        %% GetAlgebraicDist
        function algebraicDist = GetAlgebraicDist(points, centerPoint, radii)

algebraicDist = ((points(:,1)-centerPoint(1))/radii(1)).^2 ...
              + ((points(:,2)-centerPoint(2))/radii(2)).^2 ...
              + ((points(:,3)-centerPoint(3))/radii(3)).^2;
end
        %% Collision checking

function Collision(self,robot,nextTr,gripper)

centerPoint = [0,0,0];
radii = [3,2,1];
[X,Y,Z] = ellipsoid( centerPoint(1), centerPoint(2), centerPoint(3), radii(1), radii(2), radii(3) );
view(3);
hold on;

%
ellipsoidAtOrigin_h = surf(X,Y,Z);
% Make the ellipsoid translucent (so we can see the inside and outside points)
alpha(0.1);

%
% One side of the cube
[Y,Z] = meshgrid(-0.75:0.05:0.75,-0.75:0.05:0.75);
sizeMat = size(Y);
X = repmat(0.75,sizeMat(1),sizeMat(2));
oneSideOfCube_h = surf(X,Y,Z);

% Combine one surface as a point cloud
cubePoints = [X(:),Y(:),Z(:)];

% Make a cube by rotating the single side by 0,90,180,270, and around y to make the top and bottom faces
cubePoints = [ cubePoints ...
             ; cubePoints * rotz(pi/2)...
             ; cubePoints * rotz(pi) ...
             ; cubePoints * rotz(3*pi/2) ...
             ; cubePoints * roty(pi/2) ...
             ; cubePoints * roty(-pi/2)];         
         
% Plot the cube's point cloud         
cubeAtOigin_h = plot3(cubePoints(:,1),cubePoints(:,2),cubePoints(:,3),'r.');
cubePoints = cubePoints + repmat([2,0,-0.5],size(cubePoints,1),1);
cube_h = plot3(cubePoints(:,1),cubePoints(:,2),cubePoints(:,3),'b.');
axis equal

% 
algebraicDist = GetAlgebraicDist(cubePoints, centerPoint, radii);
pointsInside = find(algebraicDist < 1);
display(['2.4: There are ', num2str(size(pointsInside,1)),' points inside']);

% 
centerPoint = [1,1,1];
algebraicDist = GetAlgebraicDist(cubePoints, centerPoint, radii);
pointsInside = find(algebraicDist < 1);
display(['2.5: There are now ', num2str(size(pointsInside,1)),' points inside']);

% 
centerPoint = [0,0,0];
cubePointsAndOnes = [inv(transl(1,1,1)) * [cubePoints,ones(size(cubePoints,1),1)]']';
updatedCubePoints = cubePointsAndOnes(:,1:3);
algebraicDist = GetAlgebraicDist(updatedCubePoints, centerPoint, radii);
algebraicDist = GetAlgebraicDist(cubePoints, centerPoint, radii);          
pointsInside = find(algebraicDist < 1);
display(['2.6: There are now ', num2str(size(pointsInside,1)),' points inside']);

% 
centerPoint = [0,0,0];
cubePointsAndOnes = [inv(transl(1,1,1)*trotx(pi/4)) * [cubePoints,ones(size(cubePoints,1),1)]']';
updatedCubePoints = cubePointsAndOnes(:,1:3);
algebraicDist = GetAlgebraicDist(updatedCubePoints, centerPoint, radii);
pointsInside = find(algebraicDist < 1);
display(['2.7: There are now ', num2str(size(pointsInside,1)),' points inside']);
pause(1);

% 
% try delete(cubeAtOigin_h); end;
% try delete(ellipsoidAtOrigin_h); end;
% try delete(oneSideOfCube_h); end;
% 
% L1 = Link('d',0,'a',1,'alpha',0,'qlim',[-pi pi])
% L2 = Link('d',0,'a',1,'alpha',0,'qlim',[-pi pi])
% L3 = Link('d',0,'a',1,'alpha',0,'qlim',[-pi pi])        
% robot = SerialLink([L1 L2 L3],'name','myRobot');  

% New values for the ellipsoid (guessed these, need proper model to work out correctly)
centerPoint = [0,0,0];
radii = [1,0.5,0.5];
[X,Y,Z] = ellipsoid( centerPoint(1), centerPoint(2), centerPoint(3), radii(1), radii(2), radii(3) );
for i = 1:4
    robot.points{i} = [X(:),Y(:),Z(:)];
    warning off
    robot.faces{i} = delaunay(robot.points{i});    
    warning on;
end

% robot.plot3d([0,0,0]);
% axis equal
% camlight
% robot.teach
% keyboard

% 
c = [0,0,0]
tr = robot.fkine(c);
cubePointsAndOnes = [inv(tr) * [cubePoints,ones(size(cubePoints,1),1)]']';
updatedCubePoints = cubePointsAndOnes(:,1:3);
algebraicDist = GetAlgebraicDist(updatedCubePoints, centerPoint, radii);
pointsInside = find(algebraicDist < 1);
display(['2.9: There are now ', num2str(size(pointsInside,1)),' points inside']);

% 
c = [0,0,0]
tr = zeros(4,4,robot.n+1);
tr(:,:,1) = robot.base;
L = robot.links;
for i = 1 : robot.n
    tr(:,:,i+1) = tr(:,:,i) * trotz(c(i)) * transl(0,0,L(i).d) * transl(L(i).a,0,0) * trotx(L(i).alpha);
end

% Go through each ellipsoid
for i = 1: size(tr,3)
    cubePointsAndOnes = [inv(tr(:,:,i)) * [cubePoints,ones(size(cubePoints,1),1)]']';
    updatedCubePoints = cubePointsAndOnes(:,1:3);
    algebraicDist = GetAlgebraicDist(updatedCubePoints, centerPoint, radii);
    pointsInside = find(algebraicDist < 1);
    display(['2.10: There are ', num2str(size(pointsInside,1)),' points inside the ',num2str(i),'th ellipsoid']);
end

keyboard
end
    end  
end

