classdef DualUR3Simulation < handle
    %> A demonstration of two UR3 Robots Arms working together to perfom
    %> automated assembly tasks of electronics equipment.
    %> Latest Revision - 5 May 2020
    %> Student Number: 11686122
    
    properties
        %> Robot model
        model;
        left = 'robotL';
        right = 'robotR';
        
        %> Dimensions of the workspace in regard to the entire simulation
        %space encompassing table, robots, mesh guards, etc.
        workspace = [-1 1 -1.5 1.5 0 2];
        
        %> Transform for centre of enviroment
        centreTr = eye(4);
        
        %> Tranformation Matrix for LH UR3 Robot Arm
        UR3LTr = transl([0.5,0,0.965]) * trotz(0 * pi/180);
        
        %> Tranformation Matrix for RH UR3 Robot Arm
        UR3RTr = transl([-0.5,0,0.965]) * trotz(180 * pi/180);
        
        endTr = transl([0,0,-0.035]) * trotz(0 * pi/180);
        
        %> Create a vector of initial joint angles to have the arm point straight up
        qUp = [0 -pi/2 0 -pi/2 0 0];
        
        %> Create a vector of initial joint angles to have the arm in a resting position
        qRest = [0 -3*pi/4 3*pi/4 0 0 0];
        
        %> Implement a multipler to speed up or slow down the movements of
        %the robots.
        speed_multiplier = 8;
        
        %> Flag to indicate if gripper is used
        useGripper = false;
        
    end
    
    methods%%...structors for UR3 robot simulation
        
        function self = DualUR3Simulation()
            %> if gripper is present include
            if nargin < 2
                useGripper = false;
            end
            self.useGripper = useGripper;
            
            leftDrop = [1];
            rightDrop = [1];
            floorDrop = [1];
            
            GetEnvironment (self,leftDrop,rightDrop,floorDrop);
            
            %> Import WorkBench
            workBench = GetObject(self,'WorkBench',self.centreTr,transl([0,0,0]),trotz(90 * pi/180));
            
            %> Import protective guard mesh screens
            guardMesh1 = GetObject(self,'WorkBenchMeshGuard',workBench.Tr, ...
                transl([0,-0.592,0.960]),trotz(0 * pi/180));
            
            guardMesh2 = GetObject(self,'WorkBenchMeshGuard',workBench.Tr, ...
                transl([0,0.592,0.960]),trotz(180 * pi/180));
            
            %> Import Small crates for incoming electronics components
            smallCrate1 = GetObject(self,'SmallCrate',workBench.Tr, ...
                transl([0.6,0.4,0.970]),trotz(90 * pi/180));
            
            smallCrate2 = GetObject(self,'SmallCrate',workBench.Tr, ...
                transl([-0.6,0.4,0.970]),trotz(90 * pi/180));
            
            smallCrate3 = GetObject(self,'SmallCrate',workBench.Tr, ...
                transl([-0.6,-0.4,0.970]),trotz(90 * pi/180));
            
            %> Import Large crate for outgoing electronics components
            largeCrate = GetObject(self,'LargeCrate',workBench.Tr, ...
                transl([0.80,-0.10,0.960]),trotz(90 * pi/180));
            
            %> Load Circuit Board Components and place in seperate small
            %crates for assembly
            topHousing = GetObject(self,'SafeCoCircuitBoardHousingTop',smallCrate1.Tr, ...
                transl([0.06,-0.01,0.1]),trotx(180 * pi/180));
            
            bottomHousing = GetObject(self,'SafeCoCircuitBoardHousingBottom',smallCrate2.Tr, ...
                transl([0.04,-0.01,0.05]),trotx(180 * pi/180));
            
            circuitBoard = GetObject(self,'SafeCoCircuitBoard',smallCrate3.Tr, ...
                transl([0.06,-0.01,0.01]),trotx(180 * pi/180));
            
            %> Get the LH UR3 Model
            robotL = self.GetUR3Robot(self.left,workBench);
            
            %> Get the RH UR3 Model
            robotR = self.GetUR3Robot(self.right,workBench);
            
            %> Plot the LH UR3
            self.PlotAndColourRobot(robotL,self.qRest);%robot,workspace);
            
            %> Plot the RH UR3
            self.PlotAndColourRobot(robotR,self.qRest);%robot,workspace);
            
            %> Run through prequisite q file and ensure both UR3 are
            %operting properly as a test.
            self.DisplayMode(robotL,robotR);
            
            %robotL.teach()
            
            %input('Finished exercises 1, press enter to continue')
            assemblyPoint = Assembly(self);
            
            dropoffPoint = Dropoff(self);
            
            collision = [];
            
            grabL = [1];
            grabR = [1];
            self.MoveRobot(robotL,topHousing,collision, workBench,grabL,topHousing);
            grabL = [];
            self.MoveRobot(robotR,circuitBoard,collision, workBench,grabR,circuitBoard);
            grabR = [];
            self.MoveRobot(robotL,topHousing,collision, workBench,grabL,assemblyPoint);
            grabL = [1];
            self.MoveRobot(robotR,circuitBoard,collision, workBench,grabR,assemblyPoint);
            grabR = [1];
            self.MoveRobot(robotR,bottomHousing,collision, workBench,grabR,bottomHousing);
            grabR = [];
            self.MoveRobot(robotR,bottomHousing,collision, workBench,grabR,assemblyPoint);
            grabR = [1];
            self.MoveRobot(robotL,topHousing,collision, workBench,grabL,dropoffPoint);
            grabL = [1];
            
        end
        
        
        
        %% GetEnviroment
        % Clear all previous figures, set axis, and activate or deactivate surroundings
        function GetEnvironment (self,leftDrop,rightDrop,floorDrop)
            
            % clear all windows and dock figure
            clf;
            clc;
            set(0,'DefaultFigureWindowStyle', 'docked')
            
            % show axis labels, square figure and hold for future plots
            xlabel('X');
            ylabel('Y');
            zlabel('Z');
            axis equal;
            hold on;
            
            if isempty(leftDrop)
            else
                imgL = imread('../CAD Parts/Environment Images/BackdropLeft.jpg');                       % Load a sample image
                xImageL = [self.workspace(1:2);self.workspace(1:2)];                                     % The x data for the image corners
                yImageL = [self.workspace(4:4);self.workspace(4:4)];                                     % The y data for the image corners
                zImageL = [self.workspace(6),self.workspace(6);self.workspace(5),self.workspace(5)];     % The z data for the image corners
                surf(xImageL,yImageL,zImageL,'CData',imgL,'FaceColor','texturemap','FaceLighting','none');
            end
            
            if isempty(rightDrop)
            else
                imgR = imread('../CAD Parts/Environment Images/BackdropRight.jpg');
                xImageR = [self.workspace(2:2);self.workspace(2:2)];
                yImageR = [self.workspace(3:4);self.workspace(3:4)];
                zImageR = [self.workspace(6),self.workspace(6);self.workspace(5),self.workspace(5)];
                surf(xImageR,yImageR,zImageR,'CData',imgR,'FaceColor','texturemap','FaceLighting','none');
            end
            
            if isempty(floorDrop)
            else
                imgB = imread('../CAD Parts/Environment Images/BackdropBottom.jpg');
                xImageB = [self.workspace(1:2);self.workspace(1:2)];
                yImageB = [self.workspace(4:4);self.workspace(3:3)];
                zImageB = [self.workspace(5),self.workspace(5);self.workspace(5),self.workspace(5)];
                surf(xImageB,yImageB,zImageB,'CData',imgB,'FaceColor','texturemap','FaceLighting','none');
            end
        end
        
        %% GetUR3Robot
        % Given a name (optional), create and return a UR5 robot model
        function model = GetUR3Robot(self,name,obj)
            if nargin < 2
                Create a unique name (ms timestamp after 1ms pause)
                pause(0.000001);
                name = ['UR_3_',datestr(now,'yyyymmddTHHMMSSFFF')];
            end
            
            % Define the DH Parameters to create the Kinematic model - UR3 arm
            L1 = Link('d',0.1708,'a',0,'alpha',-pi/2,'offset',pi,'qlim',[deg2rad(-360),deg2rad(360)]);
            L2 = Link('d',0.120,'a',-0.244,'alpha',0,'offset',pi,'qlim',[deg2rad(-180),deg2rad(180)]);
            L3 = Link('d',-0.093,'a',-0.213,'alpha',0,'offset',0,'qlim',[deg2rad(-170),deg2rad(170)]);
            L4 = Link('d',0.083,'a',00,'alpha',pi/2,'offset',0,'qlim',[deg2rad(-360),deg2rad(360)]);
            L5 = Link('d',0.083,'a',0,'alpha',-pi/2,'offset',0,'qlim',[deg2rad(-360),deg2rad(360)]);
            L6 = Link('d',0.082,'a',0,'alpha',0,'offset',0,'qlim',[deg2rad(-360),deg2rad(360)]);
            
            % Generate the UR3 model which is either robotL or robotR
            model = SerialLink([L1 L2 L3 L4 L5 L6],'name',name);
            
            % move the base of the ARM to specified pose
            if name == self.left
                model.base = obj.Tr * self.UR3LTr;
            elseif name == self.right
                model.base = obj.Tr * self.UR3RTr;
            else
                model.base = obj.Tr;
            end
        end
        
        %% PlotAndColourRobot
        % Given a robot index, add the glyphs (vertices and faces) and
        % colour them in if data is available
        function PlotAndColourRobot(self,robot,q)%robot,workspace)
            
            % read in robot data from ply file
            for linkIndex = 0:robot.n
                if self.useGripper && linkIndex == robot.n
                    [ faceData, vertexData, plyData{linkIndex+1} ] = plyread(['../CAD Parts/PLY Files/UR3Link',num2str(linkIndex),'Gripper.ply'],'tri'); %#ok<AGROW>
                else
                    [ faceData, vertexData, plyData{linkIndex+1} ] = plyread(['../CAD Parts/PLY Files/UR3Link',num2str(linkIndex),'.ply'],'tri'); %#ok<AGROW>
                end
                robot.faces{linkIndex+1} = faceData;
                robot.points{linkIndex+1} = vertexData;
            end
            
            % Display robot
            robot.plot3d(q,'workspace',self.workspace)
            
            % if cam light is off turn on
            if isempty(findobj(get(gca,'Children'),'Type','Light'))
                camlight;
            end
            robot.delay = 0.000000001;
            
            % Try to correctly colour the arm (if colours are in ply file data)
            for linkIndex = 0:robot.n
                handles = findobj('Tag', robot.name);
                h = get(handles,'UserData');
                try
                    h.link(linkIndex+1).Children.FaceVertexCData = [plyData{linkIndex+1}.vertex.red ...
                        , plyData{linkIndex+1}.vertex.green ...
                        , plyData{linkIndex+1}.vertex.blue]/255;
                    h.link(linkIndex+1).Children.FaceColor = 'interp';
                catch ME_1
                    disp(ME_1);
                    continue;
                end
            end
        end
        
        %% DisplayMode
        % Display the UR passing through the movements from the UR3
        % prerequsite file
        function DisplayMode(self,robot1,robot2)
            if nargin > 3
                q = name;
            else
                load 'ur3_q.mat' q;
            end
            
            for i = 1:size(q,1)/self.speed_multiplier
                robot1.animate(q(self.speed_multiplier*i,:));
                robot2.animate(q(self.speed_multiplier*i,:));
            end
        end
        
        %% Assembly
        % create an assembly point for each of the points.
        function assemblyPoint = Assembly(self)
            assemblyPoint.Tr = transl([0,0,1.3]);
        end
        
        %% Dropoff
        % create a dropoff object to put the final assembled peices
        function dropoffPoint = Dropoff(self)
            dropoffPoint.Tr = transl([0.80,-0.10,0.980]),trotz(90 * pi/180);
        end
        %% GetObject
        % Import the ply files that were made in blender. Similiar to the
        % PlotAndColourRobot function, the objects will have associated
        % Tr, colour, vertex, faces and ply data that will be used later for collision avoidance
        function obj = GetObject(~,name, parentTr, moveTr, rotateTr)
            if nargin < 1
                obj.name = ['OBJECT_',datestr(now,'HHMM_SSFFF')];
            end
            obj.name = name;
            [obj.f,obj.v,obj.data] = plyread(['../CAD Parts/PLY Files/',name,'.ply'],'tri');
            
            % Scale the colours to be 0-to-1 (they are originally 0-to-255)
            obj.vertexColours = [obj.data.vertex.red, obj.data.vertex.green, obj.data.vertex.blue] / 255;
            
            % Get vertex count
            obj.vertexCount = size(obj.v,1);
            
            % Create a transform to describe the location (at the origin, since it's centered
            % Move the pose and  rotate if required from the parent pose
            % object
            obj.Tr = parentTr * moveTr * rotateTr;
            
            %Output the updated points for the new position
            obj.updatedPoints = (obj.Tr * [obj.v,ones(obj.vertexCount,1)]')';
            
            % Then plot the trisurf
            obj.mesh_h = trisurf(obj.f,obj.v(:,1),obj.v(:,2),obj.v(:,3) ...
                ,'FaceVertexCData',obj.vertexColours,'EdgeColor','interp','EdgeLighting','flat');
            
            %> if cam light is off turn on
            if isempty(findobj(get(gca,'Children'),'Type','Light'))
                camlight;
            end
            
            % Now update the Vertices
            obj.mesh_h.Vertices = obj.updatedPoints(:,1:3);
        end
        
        %% MoveRobot
        % Move the robot, from one pose to another keeping within
        % joint limits and following a collision free path
        function MoveRobot(self,robot,object,collision,obstacle,grab,goal)
            
            nextTr = goal.Tr * self.endTr;
            
            % Get the goal joint state
            goalJointState = robot.getpos();
            actualTr = robot.fkine(goalJointState);
            
            minDistError = sqrt(sum(sqrt(sum((actualTr - nextTr).^2)).^2));
            bestJointState = goalJointState;
            maxAllowableDistError = 0.01;
            iKineAlpha = 0.9;
            
            while maxAllowableDistError < minDistError && 0 < iKineAlpha
                candidateJointState = robot.ikine(nextTr,bestJointState,[1,1,1,1,1,1],'alpha',iKineAlpha,'ilimit',50);
                
                % Fix joint limits if needed
                if ~all(robot.qlim(:,1) < candidateJointState'  &  candidateJointState' < robot.qlim(:,2))
                    lessThanMinIndex = candidateJointState' < robot.qlim(:,1);
                    greaterThanMaxIndex = robot.qlim(:,2) < candidateJointState';
                    candidateJointState(lessThanMinIndex) = robot.qlim(lessThanMinIndex,1) + 5*pi/180;
                    candidateJointState(greaterThanMaxIndex) = robot.qlim(greaterThanMaxIndex,2) - 5*pi/180;
                    disp('Not in joint limits, so fixing');
                end
                
                actualTr = robot.fkine(candidateJointState);
                iKineAlpha = iKineAlpha - 0.0002;
                currentDistError = sqrt(sum(sqrt(sum((actualTr - nextTr).^2)).^2));
                % If less than the best so far and within joint limits
                if  currentDistError < minDistError
                    minDistError = currentDistError;
                    bestJointState = candidateJointState;
                end
            end
            
            % If error is small enough then use it otherwise stay put
            if minDistError < maxAllowableDistError
                goalJointState = bestJointState;
                % If all zero already then choose a random pose
            elseif all(robot.getpos() == 0)
                goalJointState = (rand(1,6)-0.5) * 30*pi/180;
                % If not already zero and all current solutions are poor then go to zero
            else
                goalJointState = zeros(1,6);
            end
            
            % Fix goal pose back to a small step away from the min/max joint limits
            fixIndexMin = goalJointState' < robot.qlim(:,1);
            goalJointState(fixIndexMin) = robot.qlim(fixIndexMin,1) + 1*pi/180;
            fixIndexMax = robot.qlim(:,2) < goalJointState';
            goalJointState(fixIndexMax) = robot.qlim(fixIndexMax,2) - 1*pi/180;
            jSteps = 20;
            q1 = robot.getpos();
            q2 = goalJointState;
            
            % run animation without collision avoidance
            if isempty(collision)
                
                %Determine the number of jSteps required through a qunitic
                %polynomial profile to acheive <1 degree movements.
                while ~isempty(find(1 < abs(diff(rad2deg(jtraj(q1,q2,jSteps)))),1))
                    jSteps = jSteps + 1;
                end
                
                % Get a trajectory by Quintic Polynomial Profile
                % create trajectory of goal steps to aheive the moment
                %jointTrajectory = jtraj(q1,q2,jSteps);
                
                % OR the better way...  Get a trajectory by Trapezoidal Profile
                % First, create the scalar function that varies smoothly from
                % state 1 to 2
                trap_s = lspb(0,1,jSteps);
                
                % Create memory allocation for variables
                qMatrix = nan(jSteps,6);
                
                % Generate interpolated joint angles
                for i = 1:jSteps
                    qMatrix(i,:) = (1-trap_s(i))*q1 + trap_s(i)*q2;
                end
                for i = 1:jSteps
                    
                    %Animate the robot arm
                    robot.animate(qMatrix(i,:));
                    
                    
                    if isempty(grab)
                        
                        %Update the object pose to follow the end effector
                        object.Tr = self.endTr * robot.fkine(qMatrix(i,:));
                        
                        %Output the updated points for the new position
                        object.updatedPoints = (object.Tr * [object.v,ones(object.vertexCount,1)]')';
                        object.mesh_h.Vertices = object.updatedPoints(:,1:3);
                        drawnow();
                    end
                end
                
            else
                
                % determine all possible points of collision and plot a
                % random path to the goal joint state
                
                % Check object vertex and faces for a particular object and check
                % if the robot model collides
                f = obstacle.f;
                v = obstacle.v;
                
                faceNormals = zeros(size(f,1),3);
                for faceIndex = 1:size(f,1)
                    v1 = v(f(faceIndex,1)',:);
                    v2 = v(f(faceIndex,2)',:);
                    v3 = v(f(faceIndex,3)',:);
                    faceNormals(faceIndex,:) = unit(cross(v2-v1,v3-v1));
                end
                
                %Randomly select waypoints if collision occurs
                robot.animate(q1);
                
                qWaypoints = [q1;q2];
                isCollision = true;
                checkedTillWaypoint = 1;
                qMatrix = [];
                
                while (isCollision)
                    startWaypoint = checkedTillWaypoint;
                    for i = startWaypoint:size(qWaypoints,1)-1
                        qMatrixJoin = InterpolateWaypointRadians(self,qWaypoints(i:(i+1),:),deg2rad(5));
                        if ~IsCollision(self,robot,qMatrixJoin,f,v,faceNormals)
                            qMatrix = [qMatrix; qMatrixJoin]; %#ok<AGROW>
                            robot.animate(qMatrixJoin);
                            size(qMatrix)
                            isCollision = false;
                            checkedTillWaypoint = i+1;
                            % Now try and join to the final goal (q2)
                            qMatrixJoin = InterpolateWaypointRadians(self,[qMatrix(end,:); q2],deg2rad(5));
                            if ~IsCollision(self,robot,qMatrixJoin,f,v,faceNormals)
                                qMatrix = [qMatrix;qMatrixJoin]; %#ok<AGROW>
                                % Reached goal without collision, so break out
                                break;
                            end
                        else
                            
                            % Randomly pick a pose that is not in collision
                            qRand = (2 * rand(1,6) - 1) * pi;
                            while IsCollision(self,robot,qRand,f,v,faceNormals)
                                qRand = (2 * rand(1,6) - 1) * pi;
                            end
                            qWaypoints =[ qWaypoints(1:i,:); qRand; qWaypoints(i+1:end,:)];
                            isCollision = true;
                            break;
                        end
                    end
                end
                
                
                for i = 1:jSteps
                    
                    %Animate the robot arm
                    robot.animate(qMatrix(i,:));
                    
                    if isempty(grab)
                        
                        %Update the object pose to follow the end effector
                        object.Tr = self.endTr * robot.fkine(qMatrix(i,:));
                        
                        %Output the updated points for the new position
                        object.updatedPoints = (object.Tr * [object.v,ones(object.vertexCount,1)]')';
                        object.mesh_h.Vertices = object.updatedPoints(:,1:3);
                        drawnow();
                    end
                    
                end
            end
        end
        
        
        %% IsIntersectionPointInsideTriangle
        % Given a point which is known to be on the same plane as the triangle
        % determine if the point is
        % inside (result == 1) or
        % outside a triangle (result ==0 )
        function result = IsIntersectionPointInsideTriangle(self,intersectP,triangleVerts)
            
            u = triangleVerts(2,:) - triangleVerts(1,:);
            v = triangleVerts(3,:) - triangleVerts(1,:);
            
            uu = dot(u,u);
            uv = dot(u,v);
            vv = dot(v,v);
            
            w = intersectP - triangleVerts(1,:);
            wu = dot(w,u);
            wv = dot(w,v);
            
            D = uv * uv - uu * vv;
            
            % Get and test parametric coords (s and t)
            s = (uv * wv - vv * wu) / D;
            if (s < 0.0 || s > 1.0)        % intersectP is outside Triangle
                result = 0;
                return;
            end
            
            t = (uv * wu - uu * wv) / D;
            if (t < 0.0 || (s + t) > 1.0)  % intersectP is outside Triangle
                result = 0;
                return;
            end
            
            result = 1;                      % intersectP is in Triangle
        end
        
        %% IsCollision
        % This is based upon the output of questions 2.5 and 2.6
        % Given a robot model (robot), and trajectory (i.e. joint state vector) (qMatrix)
        % and triangle obstacles in the environment (faces,vertex,faceNormals)
        function result = IsCollision(self,robot,qMatrix,faces,vertex,faceNormals)
            
            result = false;
            
            for qIndex = 1:size(qMatrix,1)
                % Get the transform of every joint (i.e. start and end of every link)
                tr = GetLinkPoses(self,qMatrix(qIndex,:), robot);
                
                % Go through each link and also each triangle face
                for i = 1 : size(tr,3)-1
                    for faceIndex = 1:size(faces,1)
                        vertOnPlane = vertex(faces(faceIndex,1)',:);
                        [intersectP,check] = LinePlaneIntersection(self,faceNormals(faceIndex,:),vertOnPlane,tr(1:3,4,i)',tr(1:3,4,i+1)');
                        if check == 1 && IsIntersectionPointInsideTriangle(self,intersectP,vertex(faces(faceIndex,:)',:))
                            plot3(intersectP(1),intersectP(2),intersectP(3),'g*');
                            disp('Intersection');
                            result = true;
                        end
                    end
                end
            end
        end
        
        %% GetLinkPoses
        % q - robot joint angles
        % robot -  seriallink robot model
        % transforms - list of transforms
        function [ transforms ] = GetLinkPoses(self, q, robot)
            
            links = robot.links;
            transforms = zeros(4, 4, length(links) + 1);
            transforms(:,:,1) = robot.base;
            
            for i = 1:length(links)
                L = links(1,i);
                
                current_transform = transforms(:,:, i);
                
                current_transform = current_transform * trotz(q(1,i) + L.offset) * ...
                    transl(0,0, L.d) * transl(L.a,0,0) * trotx(L.alpha);
                transforms(:,:,i + 1) = current_transform;
            end
        end
        
        %% FineInterpolation
        % Use results from Q2.6 to keep calling jtraj until all step sizes are
        % smaller than a given max steps size
        function qMatrix = FineInterpolation(self,q1,q2,maxStepRadians)
            if nargin < 3
                maxStepRadians = deg2rad(1);
            end
            
            jSteps = 2;
            
            while ~isempty(find(maxStepRadians < abs(diff(jtraj(q1,q2,jSteps))),1))
                jSteps = jSteps + 1;
            end
            
            %Get a trajecotry by the Quinitc Polynomial Profile
            %qMatrix = jtraj(q1,q2,jSteps);
            
            %Get a trajectory by Trapezoidal Profile
            % First, create the scalar function that varies smoothly from
            % state 1 to 2
            trap_s = lspb(0,1,jSteps);
            
            % Create memory allocation for variables
            qMatrix = nan(jSteps,6);
            
            % Generate interpolated joint angles
            for i = 1:jSteps
                qMatrix(i,:) = (1-trap_s(i))*q1 + trap_s(i)*q2;
            end
        end
        
        %% InterpolateWaypointRadians
        % Given a set of waypoints, finely intepolate them
        function qMatrix = InterpolateWaypointRadians(self,waypointRadians,maxStepRadians)
            if nargin < 2
                maxStepRadians = deg2rad(1);
            end
            
            qMatrix = [];
            for i = 1: size(waypointRadians,1)-1
                qMatrix = [qMatrix ; FineInterpolation(self,waypointRadians(i,:),waypointRadians(i+1,:),maxStepRadians)]; %#ok<AGROW>
            end
        end
        
    end
end